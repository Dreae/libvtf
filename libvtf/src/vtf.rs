use std::io::{Write, Error};
use byteorder::{LittleEndian, WriteBytesExt};

#[derive(Clone)]
pub enum VTFImageFormat {
    None = -1,
    RGBA8888 = 0,
    ABGR8888,
    RGB888,
    BGR888,
    RGB565,
    I8,
    IA88,
    P8,
    A8,
    RGB888Bluescreen,
    BGR888Bluescreen,
    ARGB8888,
    BGRA8888,
    DXT1,
    DXT3,
    DXT5,
    BGRX8888,
    BGR565,
    BGRX5551,
    BGRA4444,
    DXT1OneBitAlpha,
    BGRA5551,
    UV88,
    UVWQ8888,
    RGBA16161616F,
    RGBA16161616,
    UVLX8888,
}

pub struct VTFHeader {
    signature: &'static str,
    version: [u32; 2],
    header_size: u32,
    width: u16,
    height: u16,
    flags: u32,
    frames: u16,
    first_frame: u16,
    reflectivity: [f32; 3],
    bumpmap_scale: u32,
    high_res_image_format: VTFImageFormat,
    mipmap_count: u8,
    low_res_image_format: VTFImageFormat,
    low_res_image_width: u8,
    low_res_image_height: u8,
    depth: u16,
    num_resources: u32,
}

impl VTFHeader {
    pub fn write<T: Write>(&self, writer: &mut T) -> Result<(), Error> {
        writer.write(self.signature.as_bytes())?;
        
        writer.write_u32::<LittleEndian>(self.version[0])?;
        writer.write_u32::<LittleEndian>(self.version[1])?;

        writer.write_u32::<LittleEndian>(self.header_size)?;

        writer.write_u16::<LittleEndian>(self.width)?;
        writer.write_u16::<LittleEndian>(self.height)?;

        writer.write_u32::<LittleEndian>(self.flags)?;
        
        writer.write_u16::<LittleEndian>(self.frames)?;
        writer.write_u16::<LittleEndian>(self.first_frame)?;

        writer.write(&[0u8; 4])?; // First padding

        writer.write_f32::<LittleEndian>(self.reflectivity[0])?;
        writer.write_f32::<LittleEndian>(self.reflectivity[1])?;
        writer.write_f32::<LittleEndian>(self.reflectivity[2])?;

        writer.write(&[0u8; 4])?; // Padding

        writer.write_u32::<LittleEndian>(self.bumpmap_scale)?;

        writer.write_u32::<LittleEndian>(self.high_res_image_format.clone() as u32)?;

        writer.write_u8(self.mipmap_count)?;

        writer.write_u32::<LittleEndian>(self.low_res_image_format.clone() as u32)?;
        writer.write_u8(self.low_res_image_width)?;
        writer.write_u8(self.low_res_image_height)?;

        writer.write_u16::<LittleEndian>(self.depth)?;

        writer.write(&[0u8; 3])?; // Padding
        writer.write_u32::<LittleEndian>(self.num_resources)?;

        Ok(())
    }
}