use std::path::Path;
use std::fs::File;
use std::io::{Error, BufReader};

use jpeg_decoder::{Error as JpegError, Decoder as JpegDecoder, PixelFormat as JpegPixelFormat};
use png::{Decoder as PngDecoder, DecodingError as PngError, ColorType};

pub enum ImageParseError {
    JpegDecodeError(JpegError),
    PngDecodeError(PngError),
    IoError(Error)
}

impl From<JpegError> for ImageParseError {
    fn from(err: JpegError) -> ImageParseError {
        ImageParseError::JpegDecodeError(err)
    }
}

impl From<Error> for ImageParseError {
    fn from(err: Error) -> ImageParseError {
        ImageParseError::IoError(err)
    }
}

impl From<PngError> for ImageParseError {
    fn from(err: PngError) -> ImageParseError {
        ImageParseError::PngDecodeError(err)
    }
}

pub enum PixelFormat {
    Grayscale,
    Indexed,
    GrayscaleAlpha,
    RGB,
    RGBA,
    L8,
    CMYK32
}

impl From<JpegPixelFormat> for PixelFormat {
    fn from(format: JpegPixelFormat) -> PixelFormat {
        match format {
            JpegPixelFormat::CMYK32 => PixelFormat::CMYK32,
            JpegPixelFormat::L8 => PixelFormat::L8,
            JpegPixelFormat::RGB24 => PixelFormat::RGB
        }
    }
}

impl From<ColorType> for PixelFormat {
    fn from(ty: ColorType) -> PixelFormat {
        match ty {
            ColorType::RGB => PixelFormat::RGB,
            ColorType::RGBA => PixelFormat::RGBA,
            ColorType::Grayscale => PixelFormat::Grayscale,
            ColorType::GrayscaleAlpha => PixelFormat::GrayscaleAlpha,
            ColorType::Indexed => PixelFormat::Indexed
        }
    }
}

pub struct Pixels {
    pixels: Vec<u8>,
    width: u32,
    height: u32,
    pixel_format: PixelFormat,
}

impl Pixels {
    pub fn from_jpeg<T: AsRef<Path>>(path: T) -> Result<Pixels, ImageParseError> {
        let file = File::open(path)?;
        let mut decoder = JpegDecoder::new(BufReader::new(file));
        let pixels = decoder.decode()?;
        let metadata = decoder.info().unwrap();

        Ok(Pixels {
            pixels,
            width: metadata.width as u32,
            height: metadata.height as u32,
            pixel_format: metadata.pixel_format.into()
        })
    }

    pub fn from_png<T: AsRef<Path>>(path: T) -> Result<Pixels, ImageParseError> {
        let file = File::open(path)?;
        let decoder = PngDecoder::new(file);
        let (info, mut reader) = decoder.read_info()?;
        let mut pixels = Vec::with_capacity(info.buffer_size());
        reader.next_frame(&mut pixels)?;

        Ok(Pixels {
            pixels,
            width: info.width,
            height: info.height,
            pixel_format: info.color_type.into()
        })
    }
}